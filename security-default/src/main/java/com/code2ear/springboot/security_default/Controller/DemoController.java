package com.code2ear.springboot.security_default.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DemoController {
	@GetMapping("/")
	public String showDashboard() {
		return "home";
	}
}
