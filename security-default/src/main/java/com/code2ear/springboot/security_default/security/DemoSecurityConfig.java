package com.code2ear.springboot.security_default.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.client.HttpClientErrorException.Forbidden;

@Configuration
public class DemoSecurityConfig {
	@Bean
	public InMemoryUserDetailsManager userDetailsManager() {
		UserDetails rayyanDetails = User.builder().username("rayyan").password("{noop}qwerty")
				.roles("EMPLOYEE", "MANAGER", "ADMIN").build();
		UserDetails misbahDetails = User.builder().username("misbah").password("{noop}qwerty")
				.roles("EMPLOYEE", "MANAGER").build();
		UserDetails hamadDetails = User.builder().username("hamad").password("{noop}qwerty").roles("EMPLOYEE").build();
		return new InMemoryUserDetailsManager(rayyanDetails, misbahDetails, hamadDetails);
	}

	// Refer the custom login form
	// TODO: '/showMyLoginPage'
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeHttpRequests(configurer -> configurer.anyRequest().authenticated()).formLogin(
				form -> form.loginPage("/showMyLoginPage").loginProcessingUrl("/authenticateTheUser").permitAll())
				.logout(logout -> logout.permitAll());
		return httpSecurity.build();
	}
}
